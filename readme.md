# Meshing via NetGen for VTK

This is a remote module for VTK that provides
tetrahedral volume meshing of polydata surfaces.

The test serves as an example of how to use it.
The principal is simple: pass a closed vtkPolyData
object composed of only vertex, line, and triangle
cells into the vtkNetGenVolume filter; it will
return a vtkUnstructuredGrid containing only tetrahedra.

## Future directions

+ Mesh size parameters will be accessible as member
  variables accepted by the vtkNetGenVolume.
+ A mesh sizing field will be accepted as vtkImage
  data on the filter's second input port. Each point
  in the image's active scalars must be the target
  local element size.
+ Arrays on the input polydata will be used to
  specify regions/materials to either side of each
  triangle as well as boundary condition sets and
  other markers. At least the region/material markers
  will be passed to the output tetrahedra.
